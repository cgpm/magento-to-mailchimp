<?php
/**
 * Created by PhpStorm.
 * User: gianlucaporta
 * Date: 11/01/17
 * Time: 15:47
 */
class Gp_Mailchimp_Model_Observer{
    public function sincMailChimp(Varien_Event_Observer $observer){
        if(Mage::getStoreConfig('mailchimp/general/active')!=1)
            return;
        $MailChimp = new Gp_MailChimp(Mage::getStoreConfig('mailchimp/general/apikey',Mage::app()->getStore()->getStoreId()));

        $result = $MailChimp->get('lists');
        $nameList = Mage::getStoreConfig('mailchimp/general/list',Mage::app()->getStore()->getStoreId());
        foreach ($result['lists'] as $list) {
            if($list['name'] == $nameList){
                $list_id = $list['id'];
                break;
            }
        }
        $mail = $observer->getEvent()->getSubscriber()->getSubscriberEmail();
        if($observer->getEvent()->getSubscriber()->getSubscriberStatus()==1)
            $this->add($MailChimp,$list_id,$mail);
        else
            $this->remove($MailChimp,$list_id,$mail);


    }
    public function add($MailChimp,$list_id,$mail){
        $customer = Mage::getModel("customer/customer")->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
        $customer->loadByEmail($mail);
        if($customer->getId()!='')
            $data['merge_fields'] = array('FNAME' => $customer->getFirstname(), 'LNAME' => $customer->getLastname());
        $data['email_address'] = $mail;
        $data['status'] = 'subscribed';

        $result = $MailChimp->post("lists/$list_id/members", $data);
        if($result['status']!=200 && $result['status']!=404) {
            $title = isset($result['title']) ? $result['title'] : '';
            $detail = isset($result['title']) ? $result['title'] : '';
            Mage::log($list_id . ': ' . $result['status'] . ' ' . $title . ' ' . $detail, null, 'Gp_Mailchimp.log');
        }
    }
    public function remove($MailChimp,$list_id,$mail){
        $subscriber_hash = $MailChimp->subscriberHash($mail);
        $MailChimp->delete("lists/$list_id/members/$subscriber_hash");
    }
    public function updateMailChimp(Varien_Event_Observer $observer){
        if(Mage::getStoreConfig('mailchimp/general/active')!=1)
            return;
        if($observer->getEvent()->getCustomer()->getIsSubscribed()!=1)
            return;

        $MailChimp = new Gp_MailChimp(Mage::getStoreConfig('mailchimp/general/apikey',Mage::app()->getStore()->getStoreId()));
        $result = $MailChimp->get('lists');
        $nameList = Mage::getStoreConfig('mailchimp/general/list',Mage::app()->getStore()->getStoreId());
        foreach ($result['lists'] as $list) {
            if($list['name'] == $nameList){
                $list_id = $list['id'];
                break;
            }
        }

        $mail = $observer->getEvent()->getCustomer()->getEmail();
        $merge_fields = array('FNAME' => $observer->getEvent()->getCustomer()->getFirstname(), 'LNAME' => $observer->getEvent()->getCustomer()->getLastname());
        $subscriber_hash = $MailChimp->subscriberHash($mail);
        $result = $MailChimp->patch("lists/$list_id/members/$subscriber_hash", [
            'merge_fields' => $merge_fields
        ]);
        if ($result['status'] != 200 && $result['status'] != 404) {
            $title = isset($result['title']) ? $result['title'] : '';
            $detail = isset($result['title']) ? $result['title'] : '';
            Mage::log($list_id . ': ' . $result['status'] . ' ' . $title . ' ' . $detail, null, 'Gp_Mailchimp.log');
        }
    }
    public function adminSincMailChimp(Varien_Event_Observer $observer){
        if(Mage::getStoreConfig('mailchimp/general/active')!=1)
            return;
        $MailChimp = new Gp_MailChimp(Mage::getStoreConfig('mailchimp/general/apikey',Mage::app()->getStore()->getStoreId()));

        $result = $MailChimp->get('lists');
        $nameList = Mage::getStoreConfig('mailchimp/general/list',Mage::app()->getStore()->getStoreId());
        foreach ($result['lists'] as $list) {
            if($list['name'] == $nameList){
                $list_id = $list['id'];
                break;
            }
        }
        $mail = $observer->getEvent()->getCustomer()->getEmail();
        if($observer->getEvent()->getCustomer()->getIsSubscribed()==1)
            $this->add($MailChimp,$list_id,$mail);
        else
            $this->remove($MailChimp,$list_id,$mail);
    }
}